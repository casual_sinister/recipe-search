import React from 'react';

import { Link } from "react-router-dom";


const API_KEY = "11170259d9dc01aa4d2b1398b5377fea";

class Recipe extends React.Component {
  state = {
    activeRecipe: [],
    // toggle: this.toggle.bind(this),
    // collapse: false 
  }
  // toggle() {
  //   this.setState(collapse: !(this.state.collapse) );
  // }
  componentDidMount = async () => {
    console.log("this is props", this.props);
    const title = this.props.location.state.recipe;
    const title2 = this.props.location.state.recipe2;
    console.log("these are my motus", title, title2);
    // const req = await fetch(`https://cors-anywhere.herokuapp.com/http://food2fork.com/api/search?key=${API_KEY}&q=${title}`);
    
    // const res = await req.json();
    // this.setState({ activeRecipe: res.recipes[0] });
    // console.log(this.state.activeRecipe);
   
     let tempdata; 
//    console.log("this is my name ok", recipeName, this);
    var url = `https://cors-anywhere.herokuapp.com/https://developers.zomato.com/api/v2.1/location_details?entity_type=${title2}&entity_id=${title}`;
    await fetch(url, {
      method: 'GET', // or 'PUT'
      headers:{
        'Accept': 'application/json',
        'user-key': API_KEY
      }
    })
    .then((resp) => resp.json()) // Transform the data into json
  .then(function(data) {
  //  console.log("hello man", data);
    tempdata = data;
    // Create and append the li's to the ul
    })

   this.setState({activeRecipe: tempdata});



  }
  render() {
    const recipe = this.state.activeRecipe;
    console.log("this is inside my render method", recipe);
    var restaurants = recipe.best_rated_restaurant;
    console.log("these r my rests", restaurants);
     // {restaurants.map(function(res, index){
     //                return <li key={ index }>{res.name}</li>;
     //              })}



    return (
      

    <div id="accordion">
    
    {!!restaurants && restaurants.map(function(res, index){
                    console.log('res = ', res.restaurant);
                    console.log(index, res.restaurant.name, res.restaurant.book_url);
                   
                  
 return <div className="card" key={index}>
    <div className="card-header" id="headingOne">
      <h5 className="mb-0">
        <button className="btn btn-link" data-toggle="collapse" data-target={"#collapse"+(index ? index : '')} aria-expanded="true" aria-controls={"collapse"+(index ? index : '')}>
          {res.restaurant.name}
        </button>
      </h5>
    </div>

    <div id={"collapse"+(index ? index : '')} className="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div className="card-body">
      <table> <tbody>
      <tr><td> Cuisines available here: {res.restaurant.cuisines} </td> </tr>
     <tr><td> Average cost for two: {res.restaurant.currency}{res.restaurant.average_cost_for_two} </td> </tr>
     <tr><td> Address: {res.restaurant.location.address} </td> </tr>
     <tr><td> User rating: {res.restaurant.user_rating.aggregate_rating} </td> </tr>
     <tr><td> How is it?: {res.restaurant.user_rating.rating_text} </td> </tr>
     <tr><td> No of votes: {res.restaurant.user_rating.votes} </td> </tr>
     <tr><td> <button onClick={e => window.open(res.restaurant.url, '_blank')} type="button" className="btn btn-outline-dark">Go to the restaurant page</button>  </td> </tr>

     </tbody></table>

      </div>
    </div>
  </div>
  })}

</div>
    

    );
  }
};

export default Recipe;